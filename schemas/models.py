from django.db import models
from django.urls import reverse


class Type(models.Model):
    name = models.CharField(max_length=50, unique=True)


class Schemas(models.Model):
    SEPARATOR = (
        (',', ','),
        (';', ';'),
    )
    QUOTE = (
        ('"', '"'),
        ("'", "'"),
    )

    name = models.CharField(max_length=50)
    separator = models.CharField(max_length=2, choices=SEPARATOR, default=',')
    string_quote = models.CharField(max_length=2, choices=QUOTE, default='"')
    created_at = models.DateField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('schema_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Schema'
        verbose_name_plural = 'Schemas'
        ordering = ['-created_at']


class Columns(models.Model):
    name = models.CharField(max_length=50)
    order = models.PositiveIntegerField(unique=True)
    type_id = models.ManyToManyField(Type)
    schema_id = models.ManyToManyField(Schemas)


class CSVFile(models.Model):
    name = models.CharField(max_length=50)
    schema_id = models.ManyToManyField(Schemas)